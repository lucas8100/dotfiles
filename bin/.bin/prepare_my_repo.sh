#!/bin/bash

LOGIN="lucas.fabre@epitech.eu"

blih -u $LOGIN repository create $1

blih -u $LOGIN repository setacl $1 ramassage-tek r

blih -u $LOGIN repository getacl $1

git clone git@git.epitech.eu:/$LOGIN/$1
